import React, {useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Form, Button, Container, Row, Col, ButtonToolbar} from 'react-bootstrap';
import './index.css';

const ClientForm = ({onClientAdd}) => {
    const [nouveauClient, setNouveauClient] = useState("");

    const handleChange = (event) => {
        setNouveauClient(event.currentTarget.value);
    };

    const handleSubmit = (event) => {
        event.preventDefault();

        const id = new Date().getTime();
        const nom = nouveauClient;

        onClientAdd({id, nom});

        setNouveauClient("");
    };

    return (
        <Container className="page">
            <Row>
                <Col>
                    <form onSubmit={handleSubmit}>
                        <Form.Group>
                            <Form.Control
                                size="lg"
                                value={nouveauClient}
                                onChange={handleChange}
                                type="text"
                                placeholder="Ajouter un client"/>
                        </Form.Group>
                        <Button size="lg" variant="success" className="mr-3 mt-2" type="submit">Confirmer</Button>
                    </form>
                </Col>
            </Row>
        </Container>
    );
}

export default ClientForm;