import React, {useState} from 'react';
import Client from "./Client";
import ClientForm from "./ClientForm";
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import {Container, Row, Col} from "react-bootstrap";
import Counter from './Counter';

const App = () => {
    const [clients, setClients] = useState([
        {id: 1, nom: "Khadija Sarr"},
        {id: 2, nom: "Cheikh Malainine"},
        {id: 3, nom: "Raymond Chouk"},
        {id: 4, nom: "Laetitia Julien"},
        {id: 5, nom: "Walid Haddoury"},
        {id: 6, nom: "Ikram Benyoucef"},
        {id: 7, nom: "Landing Sarr"}
    ]);


    const handleDelete = id => {
        const updatedClients = [...clients];
        const index = updatedClients.findIndex(client => client.id === id);

        updatedClients.splice(index, 1);

        setClients(updatedClients);
    };

    const handleAdd = client => {
        const updatedClients = [...clients];
        updatedClients.push(client);

        setClients(updatedClients);
    };

    const title = "Liste des clients";

    return (
        <Container className="contenu">
            <Row>
                <Col>
                    <h1 className="title">{title}</h1>
                    <ul className="font"> {clients.map(client => (
                        <Client size="lg" details={client} onDelete={handleDelete}/>
                    ))}
                    </ul>
                    <ClientForm onClientAdd={handleAdd}/>
                </Col>
            </Row>
        </Container>
    );
}

export default App;
