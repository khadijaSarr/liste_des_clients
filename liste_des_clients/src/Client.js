import React, {Component} from 'react';
import './index.css'

const Client = ({details, onDelete}) => (
    <ul className="mr-4 mt-3" >
        <button className="supr" onClick={() => onDelete(details.id)}>X</button>
        {" "} {details.nom}

    </ul>
);

export default Client;