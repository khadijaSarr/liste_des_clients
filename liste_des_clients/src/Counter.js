import React, {useState} from 'react';

const Counter = (props) => {

    const [compteur,setCompteur] = useState(0);

    const handleChange = () => {
        setCompteur (compteur + 1);
    };

    return (
        <div className="counter">
            {compteur} <button onClick={handleChange}>Incrémenter</button>
        </div>
    );
};

export default Counter;